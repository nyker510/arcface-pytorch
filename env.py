import os

SLACK_INCOMMING_URL = os.getenv('SLACK_INCOMMING_URL', None)

DATASET_DIR = '/workdir/dataset'